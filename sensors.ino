#include <DHT.h>
#include <ESP8266HTTPClient.h>
HTTPClient http;

struct _data {
  int8_t temp;
  int8_t hum;
  int16_t co2;
} data[100];

uint8_t dlen = 0;
DHT dht(0, DHT11);

String fromData(byte i) {
  String out = "{\"temp\":";
  out += data[i].temp;
  out += ",\"hum\":";
  out += data[i].hum;
  out += ",\"co2\":";
  out += data[i].co2;
  out += "}";
  return out;
}

void ts_sensors() {
  byte c = (sizeof(data) / sizeof(_data));
  byte i = dlen % c;
  data[i].co2 = co2();
  data[i].hum = dht.readHumidity();
  data[i].temp = dht.readTemperature();
  dlen++;
  if (dlen > 2 * c) dlen -= c;
}

int co2() {
  return analogRead(A0);
}

void getCurrent() {
  server.send(200, mime_json, fromData( (dlen - 1) % (sizeof(data) / sizeof(_data))));
}


void getData() {
  byte count = 255;
  byte s = (sizeof(data) / sizeof(_data));

  if (server.hasArg("count")) count = server.arg("count").toInt();
  if (count > s) count = s;
  if (count > dlen) count = dlen;

  server.setContentLength(CONTENT_LENGTH_UNKNOWN);
  server.send(200, mime_json, "[");

  for (byte n, i = count; i > 0; i--) {
    n = (s + dlen - i) % s;
    server.sendContent(fromData(n));
    if (i > 1) server.sendContent(",");
  }
  server.sendContent("]");
  server.client().stop();
}

void ts_post(){
  http.begin(CONFIG.surl);
  http.addHeader("Content-Type", "application/x-www-form-urlencoded");
  int i = (dlen - 1) % (sizeof(data) / sizeof(_data));
  String out = "temp=";
  out += data[i].temp;
  out += "&hum=";
  out += data[i].hum;
  out += "&co2=";
  out += data[i].co2;
  http.POST(out);
  http.end();  
}
