#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <time.h>
#include <FS.h>
#include <stdio.h>
#include <string.h>
#include <EEPROMAnything.h>
#include <TickerScheduler.h>

TickerScheduler ts(5);

ESP8266WebServer server(80);

struct _config {
  char ap[32];
  char apass[32];
  char user[15];
  char pass[15];
  char surl[64];
} CONFIG;


const char mime_plain[] = "text/plain";
const char mime_html[] = "text/html";
const char mime_json[] = "application/json";
const char mime_bin[] = "application/octet-stream";

const char indexPath[] = "/html/index.html.gz";

void httpBAD() {
  server.send(500, mime_json, "{\"status\":\"ERROR\"}");
}

void httpBAD(char* error) {
  String out = PSTR("{\"status\":\"ERROR\",\"message\":\"");
  out += error;
  out += PSTR("\"}");
  server.send(500, mime_json, out);
}

void httpOK() {
  server.send(200, mime_json, "{\"status\":\"OK\"}");
}

void setConf() {

  if (!server.authenticate(CONFIG.user, CONFIG.pass))
    return server.requestAuthentication();
    
  if (server.hasArg("ap")) {
    server.arg("ap").toCharArray(CONFIG.ap, sizeof(CONFIG.ap));
  }
  if (server.hasArg("apass")) {
    server.arg("apass").toCharArray(CONFIG.apass, sizeof(CONFIG.apass));
  }
  if (server.hasArg("user")) {
    server.arg("user").toCharArray(CONFIG.user, sizeof(CONFIG.user));
  }
  if (server.hasArg("pass")) {
    server.arg("pass").toCharArray(CONFIG.pass, sizeof(CONFIG.pass));
  }
  if (server.hasArg("surl")) {
    server.arg("surl").toCharArray(CONFIG.surl, sizeof(CONFIG.surl));
  }
  EEPROM_writeAnything(0, CONFIG);
  EEPROM.commit();
  httpOK();
}

void getConf() {

  if (!server.authenticate(CONFIG.user, CONFIG.pass))
    return server.requestAuthentication();
 
  String out = "{\"ap\":\"";
  out += CONFIG.ap;
  out += "\",\"apass\":\"";
  out += CONFIG.apass;
  out += "\",\"user\":\"";
  out += CONFIG.user;
  out += "\",\"pass\":\"";
  out += CONFIG.pass;
  out += "\",\"surl\":\"";
  out += CONFIG.surl;
  out += "\"}";
  server.send(200, mime_json, out);
}




void getRoot() {
  File f = SPIFFS.open(indexPath, "r");
  server.streamFile(f, mime_html);
  f.close();
}

void getNotFound() {
  String message = "<html><h1>File Not Found</h1><pre>";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "</pre></html>";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, mime_html, message);
}

void setup_client() {
  Serial.println(F("Client mode"));
  Serial.print(F("AP:\t"));
  Serial.println(CONFIG.ap);
  Serial.print(F("PASS:\t"));
  Serial.println(CONFIG.apass);

  //init wifi
  WiFi.mode(WIFI_STA);
  if (strlen(CONFIG.apass)) {
    WiFi.begin(CONFIG.ap, CONFIG.apass);
  } else {
    WiFi.begin(CONFIG.ap);
  }

  // Wait for connection
  byte n = 0;
  while (WiFi.status() != WL_CONNECTED && n < 100) {
    delay(250);
    n++;
    Serial.print(".");
  }
  Serial.println();

  //No connection
  if (n == 100) {
    WiFi.mode(WIFI_AP);
    WiFi.softAP("SmartAirSensor");
    if (MDNS.begin("esp8266")) {
      Serial.println(F("MDNS responder started"));
    }
  }
  Serial.println();

  //Print debug
  Serial.print(F("IP address: "));
  Serial.println(WiFi.localIP());
  WiFi.printDiag(Serial);
}


void setup_webserver() {

  server.on("/config-get", getConf);
  server.on("/config-set", setConf);
  server.on("/current", getCurrent);
  server.on("/data", getData);
  server.on("/", getRoot);

  server.on("/robots.txt", []() {
    return server.send(200, mime_plain, "User-agent: *\nDisallow: /");
  });

  server.on("/reboot", []() {
    httpOK();
    ESP.restart() ;
  });

  //OTA update
  server.on("/update", HTTP_POST, []() {
    if (Update.hasError()) {
      httpBAD();
    } else {
      httpOK();
    }
    ESP.restart();
  }, []() {
    HTTPUpload& upload = server.upload();
    if (upload.status == UPLOAD_FILE_START) {
      Serial.setDebugOutput(true);
      WiFiUDP::stopAll();
      Serial.printf("Update: %s\n", upload.filename.c_str());
      uint32_t maxSketchSpace = (ESP.getFreeSketchSpace() - 0x1000) & 0xFFFFF000;
      if (!Update.begin(maxSketchSpace)) { //start with max available size
        Update.printError(Serial);
      }
    } else if (upload.status == UPLOAD_FILE_WRITE) {
      if (Update.write(upload.buf, upload.currentSize) != upload.currentSize) {
        Update.printError(Serial);
      }
    } else if (upload.status == UPLOAD_FILE_END) {
      if (Update.end(true)) { //true to set the size to the current progress
        Serial.printf("Update Success: %u\nRebooting...\n", upload.totalSize);
      } else {
        Update.printError(Serial);
      }
      Serial.setDebugOutput(false);
    }
    yield();
  });

  //upload index file
  server.on("/update-index", HTTP_POST, httpOK, []() {
    static File fsUploadFile;
    HTTPUpload& upload = server.upload();
    if (upload.status == UPLOAD_FILE_START) {
      fsUploadFile = SPIFFS.open(indexPath, "w");
    } else if (upload.status == UPLOAD_FILE_WRITE) {
      if (fsUploadFile)
        fsUploadFile.write(upload.buf, upload.currentSize);
    } else if (upload.status == UPLOAD_FILE_END) {
      if (fsUploadFile)
        fsUploadFile.close();
    }
  });

  //last
  server.onNotFound(getNotFound);


  server.begin();
  Serial.println(F("HTTP server started"));
}



void setup_scheduler() {
  ts.add(0, 15000, ts_sensors, true);
  ts.add(1, 15000, ts_post, true);
}


void setup(void) {
  Serial.begin(115200);
  Serial.println(F("\nBoot"));

  //config
  EEPROM.begin(sizeof(_config));
  EEPROM_readAnything(0, CONFIG);

  //filesystem
  Serial.println(F("Mounting FS..."));
  if (!SPIFFS.begin()) {
    Serial.println(F("Failed to mount file system"));
    return;
  }

  //Wifi
  setup_client();

  //Webserver
  setup_webserver();

  //time      GMT+3    WT
  configTime(3 * 3600, 0, "ru.pool.ntp.org", "pool.ntp.org");

  //tasks
  setup_scheduler();

  Serial.println(F("boot OK\n\n"));
}

void loop(void) {
  server.handleClient();
  ts.update();
}
